import React from "react";
import MainRouter from "./router";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { devToolsEnhancer } from "redux-devtools-extension";
import reducer from "./reducers";
import io from "socket.io-client";
import createSocketIoMiddleware from "redux-socket.io";
import socketConfiguration from "./core/socket";
import config from "../config";

const ioSocket = io.connect(config.webSocket);
const socketIoMiddleware = createSocketIoMiddleware(ioSocket, "server/");
const store = applyMiddleware(socketIoMiddleware, thunk)(createStore)(
  reducer,
  devToolsEnhancer()
);

const socket = socketConfiguration(store.dispatch, ioSocket);

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <MainRouter />
    </BrowserRouter>
  </Provider>
);

export default App;

export { store, socket };
