import React, { Component } from "react";
import { isAuthenticated } from "../helpers";
import { Redirect } from "react-router";

const witnAuth = ChildComponent => {
  class ComposedComponent extends Component {
    render() {
      if (isAuthenticated()) {
        return <ChildComponent {...this.props} />;
      } else {
        return <Redirect to="/" />;
      }
    }
  }

  return ComposedComponent;
};

export default witnAuth;
