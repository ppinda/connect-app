import { messageReceived, ADD_MESSAGE } from "../actions";

const socketConfiguration = (dispatch, socket) => {
  // make sure socket is connectd
  socket.on("connect", () => {
    console.log("Socket is connected!");
  });

  socket.on(ADD_MESSAGE, conversation => {
    console.log("my message is received", conversation);
    dispatch(messageReceived(conversation));
  });

  return socket;
};

export default socketConfiguration;
