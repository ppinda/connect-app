import { logOut } from "../../api/auth-api";

export const authenticate = (jwt, userId, cb) => {
  if (window !== undefined) {
    sessionStorage.setItem("jwt", jwt);
    sessionStorage.setItem("userId", userId);
    cb();
  }
};

export const signOut = cb => {
  if (window !== "undefined") {
    sessionStorage.removeItem("jwt");
    sessionStorage.removeItem("userId");
    logOut().then(res => {
      cb();
    });
  }
};

export const isAuthenticated = () => {
  if (window === "undefined") {
    return false;
  }

  if (sessionStorage.getItem("jwt")) {
    return sessionStorage.getItem("jwt");
  } else {
    return false;
  }
};

export const getUserIdFormLocalStorage = () => {
  if (window === "undefined") {
    return false;
  }

  if (sessionStorage.getItem("userId")) {
    return sessionStorage.getItem("userId");
  } else {
    return false;
  }
};

/**
 *
 * @param {*} conversationMember
 * @param {*} currentUSer
 */
export const getUserNameForConversation = (
  conversationMember,
  belongToUserId
) => {
  if (!conversationMember) {
    return { name: "No name" };
  }
  return conversationMember.find(item => item._id === belongToUserId);
};
