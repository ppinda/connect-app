import {
  ADD_MESSAGE,
  MESSAGE_RECEIVED,
  SIGNED_USER,
  LOG_OUT,
  INIT_CONVERSATION
} from "../actions";

export const INIT_STATE = {
  conversation: [],
  user: {},
  signOut: {}
};

export default function(state = INIT_STATE, action) {
  switch (action.type) {
    case INIT_CONVERSATION:
    case MESSAGE_RECEIVED:
    case ADD_MESSAGE:
      return {
        ...state,
        conversation: { ...state.conversation, ...action.payload }
      };
    case SIGNED_USER:
      return { ...state, user: action.payload };
    case LOG_OUT:
      return { ...state, user: {} };
    default:
      return state;
  }
}
