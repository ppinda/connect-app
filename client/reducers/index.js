import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import reducer from "./reducer";

const rootReducer = combineReducers({
  form: formReducer,
  connectPeople: reducer
});

export default rootReducer;
