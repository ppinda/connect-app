import React from "react";
import { Figure, Col, Row } from "react-bootstrap";
import AvatarPlaceholder from "../static/avatar.svg";

const Avatar = ({ name, src, active }) => (
  <Row>
    <Col xs={active ? { span: 4, offset: 8 } : {}}>
      <Figure>
        <Figure.Image width={40} height={40} alt="171x180" src={src} />
        <Figure.Caption>{name}</Figure.Caption>
      </Figure>
    </Col>
  </Row>
);

Avatar.defaultProps = {
  name: "No name",
  src: AvatarPlaceholder
};

export default Avatar;
