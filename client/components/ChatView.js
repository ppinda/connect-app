import React from "react";
import { ListGroup } from "react-bootstrap";
import Avatar from "./Avatar";
import { getUserNameForConversation } from "../core/helpers";

const ChatView = ({ messages, user, activeUser }) => {
  if (!messages) {
    return null;
  }
  return (
    <ListGroup>
      {messages.map((item, index) => (
        <div key={index}>
          <Avatar
            name={
              user && item.belongToUserId
                ? getUserNameForConversation(user, item.belongToUserId).name
                : "No name"
            }
            active={
              user && item.belongToUserId
                ? getUserNameForConversation(user, item.belongToUserId)._id ===
                  activeUser._id
                : false
            }
          />
          <ListGroup.Item>{item.text}</ListGroup.Item>
        </div>
      ))}
    </ListGroup>
  );
};

export default ChatView;
