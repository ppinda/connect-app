const signIn = credentials => {
  return fetch("/api/signin", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    credentials: "include",
    body: JSON.stringify(credentials)
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

const create = user => {
  return fetch("/api/user", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    credentials: "include",
    body: JSON.stringify(user)
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

const logOut = () => {
  return fetch("/api/signout", {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

const read = (params, credentials) => {
  return fetch("/api/user/" + params.userId, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + credentials
    }
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

export { signIn, create, logOut, read };
