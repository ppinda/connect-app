const create = (userId, credentials) => {
  return fetch(`api/conversation/${userId}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + credentials
    },
    credentials: "include",
    body: JSON.stringify(userId)
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

const addConversation = (text, userId, credentials) => {
  return fetch(`api/conversation/${userId}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + credentials
    },
    credentials: "include",
    body: JSON.stringify({ text })
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

const list = (userId, credentials) => {
  return fetch(`api/conversation/${userId}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + credentials
    },
    credentials: "include"
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

export { create, addConversation, list };
