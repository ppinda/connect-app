import React, { Component } from "react";
import { Route, BrowserRouter, browserHistory } from "react-router-dom";
import { Container } from "react-bootstrap";

import Home from "../container/Home";
import NavBar from "../container/NavBar";
import ComunicationPage from "../container/CommunicationContainer";
import SignUp from "../container/SignUp";
import SignIn from "../container/SignIn";
import withAuth from "../core/hoc/withAuth";

class Router extends Component {
  render() {
    return (
      <BrowserRouter browserHistory={browserHistory}>
        <Container>
          <Route path="*" component={NavBar} />
          <Route exact path="/" component={Home} />
          <Route exact path="/connect" component={withAuth(ComunicationPage)} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/signin" component={SignIn} />
        </Container>
      </BrowserRouter>
    );
  }
}

export default Router;
