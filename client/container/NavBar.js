import React, { Component } from "react";
import { connect } from "react-redux";
import { signOut } from "../core/helpers";
import { Navbar, Nav, Button, Row, Col } from "react-bootstrap";
import { readUser, logOutAction } from "../actions";
import { getUserIdFormLocalStorage } from "../core/helpers";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    };
  }

  logout = () => {
    signOut(() => {
      this.props.logOutAction();
      this.setState({ redirect: true });
      this.props.history.push("/");
    });
  };

  componentDidMount() {
    this.init();
  }

  init = () => {
    if (getUserIdFormLocalStorage()) {
      this.props.readUser({
        userId: getUserIdFormLocalStorage()
      });
    }
  };

  render() {
    return (
      <Row>
        <Col>
          <Navbar bg="light" expand="lg">
            <Navbar.Brand href="/">
              {this.props.userDetails ? (
                <div>Welcome to the {this.props.userDetails.name}</div>
              ) : (
                <div>Connect</div>
              )}
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="/signup">Sign Up</Nav.Link>
                <Nav.Link href="/signin">Sign In</Nav.Link>
                <Button onClick={this.logout}>Log out</Button>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </Col>
      </Row>
    );
  }
}

export default connect(
  ({ connectPeople }) => ({
    userDetails: connectPeople.user
  }),
  dispatch => ({
    readUser: params => dispatch(readUser(params)),
    logOutAction: () => dispatch(logOutAction())
  })
)(NavBar);
