import React from "react";
import { Carousel, Container, Row, Col } from "react-bootstrap";
import communicationImage1 from "../static/communication-1.jpg";
import communicationImage2 from "../static/communication-2.jpg";

export default () => {
  return (
    <Row>
      <Col md="auto">
        <Carousel>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={`${communicationImage1}?text=Second slide&bg=282c34`}
              alt="Third slide"
            />

            <Carousel.Caption>
              <h3>Second slide label</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={`${communicationImage2}?text=Third slide&bg=20232a`}
              alt="Third slide"
            />

            <Carousel.Caption>
              <h3>Third slide label</h3>
              <p>
                Praesent commodo cursus magna, vel scelerisque nisl consectetur.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </Col>
    </Row>
  );
};
