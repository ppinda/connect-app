import React, { Component } from "react";
import { connect } from "react-redux";
import { InputGroup, FormControl, Button, Row, Col } from "react-bootstrap";
import { addMessage, initConversation } from "../actions";
import Chatview from "../components/ChatView";
import { getUserIdFormLocalStorage } from "../core/helpers";

class CommunicationContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ""
    };
  }

  componentDidMount() {
    this.props.initConversation(getUserIdFormLocalStorage());
  }

  handleClick = () => {
    if (!this.state.message && this.state.message === "") {
      return;
    }
    this.props.addMessage({
      userId: getUserIdFormLocalStorage(),
      message: this.state.message
    });
    this.setState({ message: "" });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    console.log("halooo mate", this.props);

    return (
      <Row className="justify-content-md-center">
        <Col md="auto">
          <Chatview
            messages={this.props.conversation.message}
            user={this.props.conversation.users}
            activeUser={this.props.user}
          />
          <InputGroup className="mb-3">
            <FormControl
              value={this.state.message}
              placeholder="Username"
              aria-label="username"
              aria-describedby="basic-addon2"
              onChange={this.handleChange("message")}
            />
            <InputGroup.Append>
              <Button
                variant="outline-secondary"
                onClick={this.handleClick}
                disabled={!this.state.message && this.state.message === ""}
              >
                Send
              </Button>
            </InputGroup.Append>
          </InputGroup>
        </Col>
      </Row>
    );
  }
}

export default connect(
  ({ connectPeople }) => ({
    conversation: connectPeople.conversation,
    user: connectPeople.user
  }),
  dispatch => ({
    addMessage: messageData => dispatch(addMessage(messageData)),
    initConversation: userId => dispatch(initConversation(userId))
  })
)(CommunicationContainer);
