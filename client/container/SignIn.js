import React, { PureComponent } from "react";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { authenticate } from "../core/helpers";
import { signIn } from "../api/auth-api";
import { saveSignInUser } from "../actions";
import { Alert, Form, Button, Col, Card } from "react-bootstrap";

class SignIn extends PureComponent {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      error: "",
      redirect: false
    };
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleSubmit = () => {
    signIn(this.state).then(res => {
      if (res.error) {
        this.setState({ error: res.error });
        return;
      }
      this.props.saveSignInUser(res.user);

      authenticate(res.token, res.user._id, () => {
        this.setState({ redirect: true, userId: res.user._id });
      });
    });
  };

  render() {
    const { redirect, error } = this.state;
    if (redirect) {
      return <Redirect to="connect" />;
    }

    return (
      <Card style={{ width: "18rem" }}>
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">Sign In</Card.Subtitle>
          <Form inline>
            <Col>
              <Form.Group controlId="formBasicEmail">
                <Form.Control type="email" placeholder="Email" onChange={this.handleChange("email")} />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group controlId="formBasicPassword">
                <Form.Control type="password" placeholder="Password" onChange={this.handleChange("password")} />
              </Form.Group>
            </Col>
            <Col>
              <Button variant="outline-success" onClick={this.handleSubmit}>
                Login
              </Button>
            </Col>
          </Form>
        </Card.Body>
        {error && (
          <Alert dismissible variant="danger">
            <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
            <p>{this.state.error}</p>
          </Alert>
        )}
      </Card>
    );
  }
}

export default connect(
  null,
  dispatch => ({
    saveSignInUser: userDetails => dispatch(saveSignInUser(userDetails))
  })
)(SignIn);
