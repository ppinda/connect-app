import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { create } from "../api/auth-api";
import { saveSignInUser } from "../actions";
import { authenticate } from "../core/helpers";
import { Alert, Form, Button, Col, Card } from "react-bootstrap";

class SignUp extends PureComponent {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      error: "",
      redirect: false
    };
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleSubmit = () => {
    create(this.state).then(data => {
      if (data.user) {
        authenticate(data.token, data.user._id, () => {
          this.props.saveSignInUser(data.user);
          this.setState({ redirect: true });
        });
      }
      if (data.error) {
        this.setState({ error: data.error });
      }
    });
  };

  render() {
    const { redirect } = this.state;
    if (redirect) {
      return <Redirect to="/connect" />;
    }

    return (
      <Card style={{ width: "18rem" }}>
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
          <Card.Text>Some quick example text to build on the card title and make up the bulk of the card's content.</Card.Text>
          <Form inline>
            <Col>
              <Form.Group controlId="formBasicName">
                <Form.Control type="text" placeholder="Full name" onChange={this.handleChange("name")} />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group controlId="formBasicEmail">
                <Form.Control type="email" placeholder="Email" onChange={this.handleChange("email")} />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group controlId="formBasicPassword">
                <Form.Control type="password" placeholder="Password" onChange={this.handleChange("password")} />
              </Form.Group>
            </Col>
            <Col>
              <Button variant="outline-success" onClick={this.handleSubmit}>
                Login
              </Button>
            </Col>
          </Form>
          {this.state.error && (
            <Alert dismissible variant="danger">
              <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
              <p>{this.state.error}</p>
            </Alert>
          )}
        </Card.Body>
      </Card>
    );
  }
}

export default connect(
  null,
  dispatch => ({
    saveSignInUser: userDetails => dispatch(saveSignInUser(userDetails))
  })
)(SignUp);
