import { socket } from "../app";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const INIT_CONVERSATION = "INIT_CONVERSATION";
export const MESSAGE_RECEIVED = "MESSAGE_RECEIVED";
export const SIGNED_USER = "SIGNED_USER";
export const LOG_OUT = "LOG_OUT";

import { read } from "../api/auth-api";
import { list, addConversation } from "../api/conversation-api";

import { isAuthenticated } from "../core/helpers";

export const addMessage = ({ message, userId }) => dispatch => {
  addConversation(message, userId, isAuthenticated()).then(
    ({ conversation }) => {
      // socket.emit(ADD_MESSAGE, conversation);
      dispatch({ type: ADD_MESSAGE, payload: conversation });
    }
  );
};

export function messageReceived(message) {
  return { type: MESSAGE_RECEIVED, payload: message };
}

export function saveSignInUser(data) {
  return { type: SIGNED_USER, payload: data };
}

export function logOutAction() {
  return { type: LOG_OUT, payload: {} };
}

export const initConversation = userId => dispatch => {
  list(userId, isAuthenticated()).then(({ conversation }) => {
    dispatch({ type: INIT_CONVERSATION, payload: conversation });
  });
};

export const readUser = params => dispatch => {
  if (isAuthenticated()) {
    read(params, isAuthenticated()).then(data => {
      dispatch(saveSignInUser(data.user));
    });
  }
};
