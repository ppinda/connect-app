import express from "express";
import path from "path";
import bodyParser from "body-parser";
import compress from "compression";
import cors from "cors";
import helmet from "helmet";
import http from "http";
import socket from "socket.io";

import { Provider } from "react-redux";
import userRoutes from "./routes/user.route";
import Template from "./../template";

//client site modules
import React from "react";
import ReactDOMServer from "react-dom/server";
import ClientRouter from "./../client/router";
import StaticRouter from "react-router-dom/StaticRouter";
import { store } from "../client/app";

const CURRENT_WORKING_DIR = process.cwd();
const app = express();
const httpServer = http.createServer(app);
const io = socket.listen(httpServer);

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(compress());
// secure apps by setting various HTTP headers
app.use(helmet());
// enable CORS - Cross Origin Resource Sharing
app.use(cors());

app.use("/dist", express.static(path.join(CURRENT_WORKING_DIR, "dist")));
app.use("/styles", express.static(path.join(CURRENT_WORKING_DIR, "styles")));

// apend sockets on req
app.use(function(req, res, next) {
  req.io = io;
  next();
});

//setting up routs
app.use("/", userRoutes);

// TO DO: save message to mongo
io.on("connection", socket => {
  socket.on("disconnect", () => {
    console.log("user disconnected");
  });
});

app.get("*", (req, res) => {
  let markup;
  let context;
  try {
    context = {};
    markup = ReactDOMServer.renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url} context={context}>
          <ClientRouter />
        </StaticRouter>
      </Provider>
    );
  } catch (error) {
    console.log("errror", error);
  }

  if (context.ur) {
    return res.redirect(303, context.url);
  }
  return res.status(200).send(Template(markup));
});

// Catch unauthorised errors
app.use((err, req, res, next) => {
  if (err.name === "UnauthorizedError") {
    res.status(401).json({
      error: err.name + ": " + err.message
    });
  }
});

export default httpServer;
