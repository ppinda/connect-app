import express from "express";
import userCtrl from "../controllers/user.controller";
import authCtrl from "../controllers/auth.controller";
import convCtrl from "../controllers/conversation.controller";

const router = express.Router();

router
  .route("/api/user/:userId")
  .get(userCtrl.findById)
  .delete(userCtrl.remove)
  .put(userCtrl.update);

router
  .route("/api/user")
  .get(userCtrl.list)
  .post(userCtrl.create);

router.route("/api/signout").get(authCtrl.signout);
router.route("/api/signin").post(authCtrl.signin);

router
  .route("/api/conversation/:id")
  .get(convCtrl.list)
  .post(convCtrl.create)
  .put(convCtrl.addConversation);

// configure param - this method is
router.param("userId", userCtrl.findById);
router.param("id", convCtrl.findConversation);

export default router;
