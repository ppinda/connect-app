import Conversation from "../models/conversation.model";

const create = (req, res) => {
  const conversation = new Conversation(req.body);
  conversation.users.push(req.body.userId);

  conversation.save((error, conv) => {
    if (error) {
      return res.status(400).json({
        error,
        message: "Conversation can not be created"
      });
    }
    return res.status(200).json({
      success: true,
      conversation: conv
    });
  });
};

const list = (req, res) => {
  Conversation.findOne({ users: { $in: [req.conversationData.userId] } })
    .populate("users")
    .exec((err, conv) => {
      if (err) {
        return res.status(400).json({
          error: "Cant find the conversation"
        });
      }

      res.status(200).json({
        success: true,
        conversation: conv
      });
    });
};

const findConversation = (req, res, next, id) => {
  Conversation.findOne({ users: { $in: [id] } })
    .populate("users")
    .exec((err, conv) => {
      if (err) {
        return res.status(400).json({
          error: "Cant find the conversation"
        });
      }

      req.conversationData = {
        userId: id,
        conversation: conv
      };
      next();
    });
};

const addConversation = (req, res) => {
  const conversation = req.conversationData.conversation;
  const message = {
    belongToUserId: req.conversationData.userId,
    text: req.body.text
  };
  conversation.message.push(message);
  const conversationUpdate = Object.assign(conversation, req.body);

  conversationUpdate.save((err, conversation) => {
    if (err) {
      return res.status(400).json({
        error: "Cant update the conversation"
      });
    }

    req.io.emit("ADD_MESSAGE", conversation);

    return res.status(200).json({
      success: true,
      conversation
    });
  });
};

export default { create, findConversation, addConversation, list };
