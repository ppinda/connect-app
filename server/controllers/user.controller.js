import User from "../models/user.model";
import jwt from "jsonwebtoken";
import config from "./../../config";

const create = (req, res, next) => {
  const user = new User(req.body);

  user.save((err, { _id, name, email }) => {
    if (err) {
      return res.status(400).json({
        error: `Can not create user ${JSON.stringify(err)}`
      });
    }

    const token = jwt.sign(
      {
        _id
      },
      config.jwtSecret
    );

    res.cookie("t", token, {
      expire: new Date() + 9999
    });

    res.status(200).json({
      message: "User successfully created!",
      user: {
        _id,
        name,
        email
      },
      token
    });
  });
};

const findById = (req, res, next, id) => {
  User.findById({ _id: id }).exec((err, { _id, name, email }) => {
    if (err) {
      return res.status(400).json({
        error: `Can find user: ${JSON.stringify(err)}`
      });
    }
    return res.status(200).json({
      message: "User successfully found!",
      user: {
        _id,
        name,
        email
      }
    });
    // req.userData = user;
    // next();
  });
};

const list = (req, res, next) => {
  User.find((err, result) => {
    if (err) {
      return res.status(400).json({
        error: `Can find user: ${JSON.stringify(err)}`
      });
    }

    return res.status(400).json({
      message: "User successfully found!",
      users: result
    });
  });
};

const remove = (req, res) => {
  const user = req.userData;
  user.remove((err, deletedUser) => {
    if (err) {
      return res.status(400).json({
        error: `Can delete user: ${JSON.stringify(err)}`
      });
    }
    return res.status(400).json({
      message: "User has been deleted!",
      user: deletedUser
    });
  });
};

const update = (req, res, next) => {
  const updatedUser = Object.assign(req.userData, req.body);
  updatedUser.updated = Date.now();

  updatedUser.save((err, user) => {
    if (err) {
      return res.status(400).json({
        error: `Can not delete the user: ${JSON.stringify(err)}`
      });
    }

    return res.status(200).json({
      message: "User has been updated!",
      user: updatedUser
    });
  });
};

export default { create, list, findById, remove, update };
