import mongoose from "mongoose";

const ConversationSchema = new mongoose.Schema({
  message: [
    {
      text: { type: String },
      belongToUserId: {
        type: String
      }
    }
  ],
  created: {
    type: Date,
    default: Date.now
  },
  updatedConversation: {
    type: Date,
    default: Date.now
  },
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }]
});

export default mongoose.model("Conversation", ConversationSchema);
