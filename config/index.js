const config = {
  env: process.env.NODE_ENV || "development",
  port: process.env.PORT || 3000,
  webSocket: process.env.WEBSOCKET_URI || "http://localhost:3000",
  jwtSecret: process.env.JWT_SECRET || "key123",
  mongoUri:
    process.env.MONGODB_URI ||
    process.env.MONGO_HOST ||
    "mongodb://" +
      (process.env.IP || "127.0.0.1") +
      ":" +
      (process.env.MONGO_PORT || "27017") +
      "/web"
};

export default config;
